﻿using System;
using System.Windows.Forms;
using MetroFramework.Forms;
using MySql.Data.MySqlClient;

namespace ComplexCalculationOfBoilerCombustion
{
    public partial class BasicAuthorization : MetroForm
    {
        public BasicAuthorization()
        {
            InitializeComponent();
        }

        private void Autorithation_Click(object sender, EventArgs e)
        {
            var dbEnterData = @"server=127.0.0.1;user id="+Login.Text+";password="+Password.Text+";persistsecurityinfo=True;database=db";

            var myConnection = new MySqlConnection(dbEnterData);
            try
            {
                myConnection.Open();
                Connector.Connect = dbEnterData;
                var mainForm = new MainForm();
                mainForm.Show();
                Hide();
            }
            catch (Exception)
            {
                MessageBox.Show("Connection not open");
            }
            finally
            {
                myConnection.Close();
            }
        }
        
        
    }
}
