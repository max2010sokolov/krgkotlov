﻿using System;
using System.Reflection;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.IO;
using Word = Microsoft.Office.Interop.Word;
using MySql.Data.MySqlClient;


namespace ComplexCalculationOfBoilerCombustion
{
    public partial class DataViewer : MetroForm
    {
        //todo rename
        private readonly string[,] _report;
        public DataViewer(string[,] report)
        {
            InitializeComponent();
            _report = report;
        }

        private void Report_Click(object sender, EventArgs e)
        {
            const string filename = @"Шаблон.docx";
            if (Adress.Text == string.Empty || Model.Text == string.Empty || NKotla.Text == string.Empty || ZnumberK.Text == string.Empty)
            {
                MessageBox.Show("Введите данные по котлу");
            }
            else
            {
                if (!File.Exists(filename))
                {
                    MessageBox.Show("Шаблон не найден");
                }
                else
                {
                    var fileinfo = new FileInfo(filename);
                    //var savefilename = @"Режимная карта котла " + Model.Text + "№ " + NKotla.Text + Adress.Text;
                    var wordApplication = new Word.Application();
                    var sbl = wordApplication.Documents.Add(fileinfo.FullName);
                    sbl.Range().Copy();
                    
                    var doc = wordApplication.Documents.Add();
                    doc.ActiveWindow.Selection.PasteAndFormat(Word.WdRecoveryType.wdFormatOriginalFormatting);
                    sbl.Close();
                    
                    try
                    {
                        Word.Range wordRange;
                        object start = 0;
                        object end = 0;
                        var wordrange = doc.Range(ref start, ref end);
                        wordrange.Font.Name = "Times New Roman";
                        string[,] ReplaceMass = new string[5, 2]
                        {
                        {"<Adress>",Adress.Text },
                        {"<Model>",Model.Text },
                        {"<NumbK>",NKotla.Text },
                        {"<Date>",DateTime.Now.ToLongDateString() },
                        {"<Znumber>",ZnumberK.Text}
                        };


                        object replaceTypeObj;
                        object _missingObj = null;
                        replaceTypeObj = Word.WdReplace.wdReplaceAll;

                        for (int j = 0; j < 5; j++)
                        {
                            object strToFindObj = ReplaceMass[j, 0];
                            object replaceStrObj = ReplaceMass[j, 1];
                            for (int i = 1; i <= doc.Sections.Count; i++)
                            {

                                wordRange = doc.Sections[i].Range;
                                Word.Find wordFindObj = wordRange.Find;
                                object[] wordFindParameters = new object[15] { strToFindObj, _missingObj, _missingObj, _missingObj, _missingObj, _missingObj, _missingObj, _missingObj, _missingObj, replaceStrObj, replaceTypeObj, _missingObj, _missingObj, _missingObj, _missingObj };
                                wordFindObj.GetType().InvokeMember("Execute", BindingFlags.InvokeMethod, null, wordFindObj, wordFindParameters);

                            }
                        }

                        start = 186;
                        end = 187;
                        wordrange = doc.Range(ref start, ref end);

                        int rows = 2 + dataGridViever.Rows.Count;
                        int columns = 3 + dataGridViever.Columns.Count;
                        object defaultTableBehavior = Word.WdDefaultTableBehavior.wdWord9TableBehavior;
                        object autoFitBehavior = Word.WdAutoFitBehavior.wdAutoFitContent;
                        Word.Table wordtable = doc.Tables.Add(wordrange, rows, columns, ref defaultTableBehavior, ref autoFitBehavior);
                        wordtable.Range.Font.Size = 11;
                        Word.Range wordcellrange = doc.Tables[1].Cell(0, 0).Range;
                        doc.Tables[1].Rows.HeightRule = Word.WdRowHeightRule.wdRowHeightExactly;
                        doc.Tables[1].Rows.Height = 18F;
                        int indexStage = 1;
                        object begCell;
                        object endCell;

                        for (int m = 1; m <= wordtable.Rows.Count; m++)
                        {
                            for (int n = 1; n <= wordtable.Columns.Count; n++)
                            {
                                wordcellrange = wordtable.Cell(m, n).Range;
                                if (n <= 4)
                                {
                                    wordcellrange.Text = _report[m - 1, n - 1];
                                }

                                if ((m > 2) && (n > 3))
                                {
                                    wordcellrange.Text = Convert.ToString(dataGridViever.Rows[m - 3].Cells[n - 4].Value);
                                }

                                if ((m == 2) && (n > 3))
                                {
                                    wordcellrange.Text = Convert.ToString(indexStage);
                                    indexStage++;
                                }
                                if ((n == 1) || (n > 2))
                                {
                                    wordcellrange.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                                }
                            }
                        }


                        if (dataGridViever.Columns.Count > 1)
                        {
                            begCell = wordtable.Cell(1, 4).Range.Start;
                            endCell = wordtable.Cell(1, columns).Range.End;
                            wordcellrange = doc.Range(ref begCell, ref endCell);
                            wordcellrange.Select();
                            wordApplication.Selection.Cells.Merge();
                            wordcellrange.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;

                            for (int i = 1; i <= wordtable.Rows.Count; i++)
                            {
                                if ((i == 4) || (i == 5) || (i == 6) || (i == 9) || (i == 10))
                                {
                                    string text;
                                    text = wordtable.Cell(i, 4).Range.Text.Replace("\r", "");
                                    begCell = wordtable.Cell(i, 4).Range.Start;
                                    endCell = wordtable.Cell(i, columns).Range.End;
                                    wordcellrange = doc.Range(ref begCell, ref endCell);
                                    wordcellrange.Select();
                                    wordApplication.Selection.Cells.Merge();
                                    wordApplication.Selection.Cells[1].Range.Text = text;
                                    wordcellrange.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;

                                }
                            }
                        }


                        for (int i = 1; i < 4; i++)
                        {
                            begCell = wordtable.Cell(1, i).Range.Start;
                            endCell = wordtable.Cell(2, i).Range.End;
                            wordcellrange = doc.Range(ref begCell, ref endCell);
                            wordcellrange.Select();
                            wordApplication.Selection.Cells.Merge();
                            wordcellrange.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                        }

                        wordApplication.Visible = true;

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        doc.Close();
                        wordApplication.Quit();
                    }
                }
            }
        }

        private void AddDB_Click(object sender, EventArgs e)
        {
            string[] parametrs= new string [dataGridViever.Rows.Count+1];
            MySqlConnection conn = Connector.Connection();
            if ((Adress.Text == string.Empty) || (Model.Text == string.Empty) || (NKotla.Text == string.Empty) || (ZnumberK.Text == string.Empty))
            {
                MessageBox.Show("Введите данные по котлу");
            }
            else 
            {
                try
                {
                    conn.Open();
                    for (int i = 0; i < dataGridViever.Columns.Count; i++)
                    {
                        
                        for (int j = 0; j < dataGridViever.Rows.Count; j++)
                        {
                            parametrs[j] = Convert.ToString(dataGridViever.Rows[j].Cells[i].Value);
                            if (parametrs.Length - j == 0)
                            {
                                parametrs[j] = (i + 1).ToString();
                            } 
                            
                        }
                        
                        string sql = "INSERT INTO stage (power, pvodin, pvodout, gvod, tvodin, tvodout, toplivo, qgaz, pgaz, ggaz, pvozduh, tvozduh, co2, o2, co, nox, a, q2, z, q3, q5, q5nom, n, gut, qpar, date, idnamestage)" +
                        "VALUES('" + parametrs[0] + "','" + parametrs[1] + "','" + parametrs[2] + "','" + parametrs[3] + "','" + parametrs[4] + "','" + parametrs[5] + "','" + parametrs[6] + "','" + parametrs[7] + "'," +
                        "'" + parametrs[8] + "','" + parametrs[9] + "','" + parametrs[10] + "','" + parametrs[11] + "','" + parametrs[12] + "','" + parametrs[13] + "','" + parametrs[14] + "','" + parametrs[15] + "','" + parametrs[16] + "'," +
                        "'" + parametrs[17] + "','" + parametrs[18] + "','" + parametrs[19] + "','" + parametrs[20] + "','" + parametrs[21] + "','" + parametrs[22] + "','" + parametrs[23] + "','" + parametrs[24] + "','" + DateTime.Now.ToShortDateString() + "','" +Convert.ToInt32(parametrs[25])+ "');";
                        MySqlCommand command = new MySqlCommand(sql,conn);
                        command.ExecuteNonQuery();
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    conn.Close();
                }
                finally 
                {
                    conn.Close();
                }
            }
        }
    }
}
