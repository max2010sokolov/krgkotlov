﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using Word = Microsoft.Office.Interop.Word;
using MySql.Data.MySqlClient;


namespace ComplexCalculationOfBoilerCombustion
{
    public partial class FormBD : MetroForm
    {
        public FormBD()
        {
            InitializeComponent();
        }

         //this.comboBox1.DataSource = this.bindingSource1;
         //this.comboBox1.DisplayMember = "Name";
         //this.comboBox1.ValueMember = "id";
        private void FormBD_Load(object sender, EventArgs e)
        {
            
            MySqlConnection conn = Connector.Connection();
            try
            {
                //string sql = "SELECT kotel.model, kotel.invnumber,filial.naimenovaniefiliala, kotelnaya.adres FROM filial,kotel,kotelnaya;";
                string sql = "SELECT * FROM stage;";
                conn.Open();
                MySqlCommand command = new MySqlCommand(sql, conn);
                MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0];
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }


        }

    }
}
