﻿using MySql.Data.MySqlClient;


namespace ComplexCalculationOfBoilerCombustion
{
    public static class Connector
    {
        public static string Connect { get; set; }
        public static MySqlConnection Connection()
        {
            var sqlConnection = new MySqlConnection(Connect);
            return sqlConnection;
        }
    }
}




