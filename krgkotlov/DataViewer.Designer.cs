﻿
namespace ComplexCalculationOfBoilerCombustion
{
    partial class DataViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViever = new System.Windows.Forms.DataGridView();
            this.Report = new MetroFramework.Controls.MetroButton();
            this.Model = new MetroFramework.Controls.MetroTextBox();
            this.NKotla = new MetroFramework.Controls.MetroTextBox();
            this.Adress = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.ZnumberK = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.AddDB = new MetroFramework.Controls.MetroButton();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViever)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViever
            // 
            this.dataGridViever.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViever.Dock = System.Windows.Forms.DockStyle.Right;
            this.dataGridViever.Location = new System.Drawing.Point(197, 60);
            this.dataGridViever.Name = "dataGridViever";
            this.dataGridViever.Size = new System.Drawing.Size(660, 602);
            this.dataGridViever.TabIndex = 0;
            // 
            // Report
            // 
            this.Report.Location = new System.Drawing.Point(12, 638);
            this.Report.Name = "Report";
            this.Report.Size = new System.Drawing.Size(66, 24);
            this.Report.TabIndex = 1;
            this.Report.Text = "Отчет";
            this.Report.Click += new System.EventHandler(this.Report_Click);
            // 
            // Model
            // 
            this.Model.Location = new System.Drawing.Point(14, 82);
            this.Model.Name = "Model";
            this.Model.Size = new System.Drawing.Size(177, 23);
            this.Model.TabIndex = 2;
            this.Model.Text = "КВГМ 2.5-115";
            // 
            // NKotla
            // 
            this.NKotla.Location = new System.Drawing.Point(14, 130);
            this.NKotla.Name = "NKotla";
            this.NKotla.Size = new System.Drawing.Size(177, 23);
            this.NKotla.TabIndex = 3;
            this.NKotla.Text = "3";
            // 
            // Adress
            // 
            this.Adress.Location = new System.Drawing.Point(14, 178);
            this.Adress.Name = "Adress";
            this.Adress.Size = new System.Drawing.Size(177, 23);
            this.Adress.TabIndex = 4;
            this.Adress.Text = "с. Новый Некоуз";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(12, 60);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(93, 19);
            this.metroLabel1.TabIndex = 5;
            this.metroLabel1.Text = "Модель котла";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(12, 108);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(174, 19);
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Номер котла по котельной";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(14, 156);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(113, 19);
            this.metroLabel3.TabIndex = 7;
            this.metroLabel3.Text = "Адрес котельной";
            // 
            // ZnumberK
            // 
            this.ZnumberK.Location = new System.Drawing.Point(14, 226);
            this.ZnumberK.Name = "ZnumberK";
            this.ZnumberK.Size = new System.Drawing.Size(177, 23);
            this.ZnumberK.TabIndex = 8;
            this.ZnumberK.Text = "174";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(14, 204);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(155, 19);
            this.metroLabel4.TabIndex = 9;
            this.metroLabel4.Text = "Заводской номер котла";
            // 
            // AddDB
            // 
            this.AddDB.Location = new System.Drawing.Point(84, 638);
            this.AddDB.Name = "AddDB";
            this.AddDB.Size = new System.Drawing.Size(107, 24);
            this.AddDB.TabIndex = 10;
            this.AddDB.Text = "Добавить в базу";
            this.AddDB.Click += new System.EventHandler(this.AddDB_Click);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(34, 285);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(0, 0);
            this.metroLabel5.TabIndex = 11;
            // 
            // DataViever
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 682);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.AddDB);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.ZnumberK);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.Adress);
            this.Controls.Add(this.NKotla);
            this.Controls.Add(this.Model);
            this.Controls.Add(this.Report);
            this.Controls.Add(this.dataGridViever);
            this.Name = "DataViewer";
            this.Resizable = false;
            this.Text = "Предварительный отчет";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViever)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroButton Report;
        public System.Windows.Forms.DataGridView dataGridViever;
        private MetroFramework.Controls.MetroTextBox Model;
        private MetroFramework.Controls.MetroTextBox NKotla;
        private MetroFramework.Controls.MetroTextBox Adress;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox ZnumberK;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroButton AddDB;
        private MetroFramework.Controls.MetroLabel metroLabel5;
    }
}