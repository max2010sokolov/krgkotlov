﻿
namespace ComplexCalculationOfBoilerCombustion
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.tabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.TabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.Nstages = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.Stage = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.TypeBoiler = new MetroFramework.Controls.MetroToggle();
            this.dT = new MetroFramework.Controls.MetroComboBox();
            this.TabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.Pvozd = new MetroFramework.Controls.MetroTextBox();
            this.PGaz = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroButton7 = new MetroFramework.Controls.MetroButton();
            this.AlfaTextBox = new MetroFramework.Controls.MetroTextBox();
            this.TvgorTextBox = new MetroFramework.Controls.MetroTextBox();
            this.TgTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.TabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.dataGridMain = new System.Windows.Forms.DataGridView();
            this.metroButton9 = new MetroFramework.Controls.MetroButton();
            this.metroButton6 = new MetroFramework.Controls.MetroButton();
            this.unitsType = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.QnTextBox = new MetroFramework.Controls.MetroTextBox();
            this.Q5nomTextBox = new MetroFramework.Controls.MetroTextBox();
            this.NnomTextBox = new MetroFramework.Controls.MetroTextBox();
            this.QTextBox = new MetroFramework.Controls.MetroTextBox();
            this.TabPage4 = new MetroFramework.Controls.MetroTabPage();
            this.Pvodout = new MetroFramework.Controls.MetroTextBox();
            this.Pvodin = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.NextStage = new MetroFramework.Controls.MetroButton();
            this.DataViverButton = new MetroFramework.Controls.MetroButton();
            this.metroButton5 = new MetroFramework.Controls.MetroButton();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.TvodvhTextBox = new MetroFramework.Controls.MetroTextBox();
            this.GnomTextBox = new MetroFramework.Controls.MetroTextBox();
            this.GvodTextBox = new MetroFramework.Controls.MetroTextBox();
            this.GTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox10 = new MetroFramework.Controls.MetroTextBox();
            this.ButtonDB = new MetroFramework.Controls.MetroButton();
            this.metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(this.components);
            this.metroStyleExtender1 = new MetroFramework.Components.MetroStyleExtender(this.components);
            this.tabControl1.SuspendLayout();
            this.TabPage1.SuspendLayout();
            this.TabPage2.SuspendLayout();
            this.TabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMain)).BeginInit();
            this.TabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // elementHost1
            // 
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(200, 100);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = null;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.TabPage1);
            this.tabControl1.Controls.Add(this.TabPage2);
            this.tabControl1.Controls.Add(this.TabPage3);
            this.tabControl1.Controls.Add(this.TabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(20, 60);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(550, 325);
            this.tabControl1.TabIndex = 0;
            // 
            // TabPage1
            // 
            this.TabPage1.Controls.Add(this.Nstages);
            this.TabPage1.Controls.Add(this.metroLabel16);
            this.TabPage1.Controls.Add(this.metroButton1);
            this.TabPage1.Controls.Add(this.Stage);
            this.TabPage1.Controls.Add(this.metroLabel15);
            this.TabPage1.Controls.Add(this.metroLabel1);
            this.TabPage1.Controls.Add(this.metroLabel14);
            this.TabPage1.Controls.Add(this.TypeBoiler);
            this.TabPage1.Controls.Add(this.dT);
            this.TabPage1.HorizontalScrollbarBarColor = true;
            this.TabPage1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.TabPage1.Location = new System.Drawing.Point(4, 35);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Size = new System.Drawing.Size(542, 286);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.VerticalScrollbarBarColor = true;
            // 
            // Nstages
            // 
            this.Nstages.Location = new System.Drawing.Point(401, 18);
            this.Nstages.Name = "Nstages";
            this.Nstages.Size = new System.Drawing.Size(97, 25);
            this.Nstages.TabIndex = 34;
            this.Nstages.Text = "4";
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(-4, 24);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(111, 19);
            this.metroLabel16.TabIndex = 33;
            this.metroLabel16.Text = "Число режимов ";
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(437, 246);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(61, 30);
            this.metroButton1.TabIndex = 32;
            this.metroButton1.Text = "Далее";
            this.metroButton1.Click += new System.EventHandler(this.ChangeTabGo);
            // 
            // Stage
            // 
            this.Stage.Location = new System.Drawing.Point(401, 49);
            this.Stage.Name = "Stage";
            this.Stage.Size = new System.Drawing.Size(97, 25);
            this.Stage.TabIndex = 31;
            this.Stage.Text = "1";
            this.Stage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyPoint_KeyPress);
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(-4, 55);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(106, 19);
            this.metroLabel15.TabIndex = 30;
            this.metroLabel15.Text = "Номер режима ";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(-4, 85);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(135, 19);
            this.metroLabel1.TabIndex = 10;
            this.metroLabel1.Text = "Разница температур";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(-4, 108);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(64, 19);
            this.metroLabel14.TabIndex = 6;
            this.metroLabel14.Text = "Паровой";
            // 
            // TypeBoiler
            // 
            this.TypeBoiler.AutoSize = true;
            this.TypeBoiler.Location = new System.Drawing.Point(416, 110);
            this.TypeBoiler.Name = "TypeBoiler";
            this.TypeBoiler.Size = new System.Drawing.Size(80, 17);
            this.TypeBoiler.TabIndex = 5;
            this.TypeBoiler.Text = "Off";
            this.TypeBoiler.UseVisualStyleBackColor = true;
            // 
            // dT
            // 
            this.dT.FontSize = MetroFramework.MetroLinkSize.Small;
            this.dT.FormattingEnabled = true;
            this.dT.ItemHeight = 19;
            this.dT.Items.AddRange(new object[] {
            "70-95 °C",
            "70-105 °C",
            "70-115 °C"});
            this.dT.Location = new System.Drawing.Point(401, 79);
            this.dT.Name = "dT";
            this.dT.Size = new System.Drawing.Size(97, 25);
            this.dT.TabIndex = 23;
            // 
            // TabPage2
            // 
            this.TabPage2.Controls.Add(this.metroLabel17);
            this.TabPage2.Controls.Add(this.Pvozd);
            this.TabPage2.Controls.Add(this.PGaz);
            this.TabPage2.Controls.Add(this.metroLabel13);
            this.TabPage2.Controls.Add(this.metroButton2);
            this.TabPage2.Controls.Add(this.metroButton7);
            this.TabPage2.Controls.Add(this.AlfaTextBox);
            this.TabPage2.Controls.Add(this.TvgorTextBox);
            this.TabPage2.Controls.Add(this.TgTextBox);
            this.TabPage2.Controls.Add(this.metroLabel10);
            this.TabPage2.Controls.Add(this.metroLabel11);
            this.TabPage2.Controls.Add(this.metroLabel12);
            this.TabPage2.HorizontalScrollbarBarColor = true;
            this.TabPage2.Location = new System.Drawing.Point(4, 35);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Size = new System.Drawing.Size(542, 286);
            this.TabPage2.TabIndex = 3;
            this.TabPage2.VerticalScrollbarBarColor = true;
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(-4, 148);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(120, 19);
            this.metroLabel17.TabIndex = 43;
            this.metroLabel17.Text = "Давление воздуха";
            // 
            // Pvozd
            // 
            this.Pvozd.Location = new System.Drawing.Point(401, 142);
            this.Pvozd.Name = "Pvozd";
            this.Pvozd.Size = new System.Drawing.Size(97, 25);
            this.Pvozd.TabIndex = 42;
            this.Pvozd.Text = "0.35";
            // 
            // PGaz
            // 
            this.PGaz.Location = new System.Drawing.Point(401, 111);
            this.PGaz.Name = "PGaz";
            this.PGaz.Size = new System.Drawing.Size(97, 25);
            this.PGaz.TabIndex = 41;
            this.PGaz.Text = "0.7";
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(-4, 118);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(97, 19);
            this.metroLabel13.TabIndex = 40;
            this.metroLabel13.Text = "Давление газа";
            // 
            // metroButton2
            // 
            this.metroButton2.Location = new System.Drawing.Point(437, 246);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(61, 30);
            this.metroButton2.TabIndex = 39;
            this.metroButton2.Text = "Далее";
            this.metroButton2.Click += new System.EventHandler(this.ChangeTabGo);
            // 
            // metroButton7
            // 
            this.metroButton7.Location = new System.Drawing.Point(0, 246);
            this.metroButton7.Name = "metroButton7";
            this.metroButton7.Size = new System.Drawing.Size(64, 30);
            this.metroButton7.TabIndex = 33;
            this.metroButton7.Text = "Назад";
            this.metroButton7.Click += new System.EventHandler(this.ChangeTabBack);
            // 
            // AlfaTextBox
            // 
            this.AlfaTextBox.Location = new System.Drawing.Point(401, 80);
            this.AlfaTextBox.MaxLength = 4;
            this.AlfaTextBox.Name = "AlfaTextBox";
            this.AlfaTextBox.Size = new System.Drawing.Size(97, 25);
            this.AlfaTextBox.TabIndex = 34;
            this.AlfaTextBox.Text = "1.20";
            this.AlfaTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyPoint_KeyPress);
            // 
            // TvgorTextBox
            // 
            this.TvgorTextBox.Location = new System.Drawing.Point(401, 49);
            this.TvgorTextBox.Name = "TvgorTextBox";
            this.TvgorTextBox.Size = new System.Drawing.Size(97, 25);
            this.TvgorTextBox.TabIndex = 33;
            this.TvgorTextBox.Text = "20";
            this.TvgorTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyPoint_KeyPress);
            // 
            // TgTextBox
            // 
            this.TgTextBox.Location = new System.Drawing.Point(401, 18);
            this.TgTextBox.Name = "TgTextBox";
            this.TgTextBox.Size = new System.Drawing.Size(97, 25);
            this.TgTextBox.TabIndex = 32;
            this.TgTextBox.Text = "162";
            this.TgTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyPoint_KeyPress);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(-4, 24);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(208, 19);
            this.metroLabel10.TabIndex = 28;
            this.metroLabel10.Text = "Температура уходящих газов(Tg)";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(-4, 55);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(250, 19);
            this.metroLabel11.TabIndex = 29;
            this.metroLabel11.Text = "Температура воздуха на горелку(Tvgor)";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(-4, 86);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(225, 19);
            this.metroLabel12.TabIndex = 30;
            this.metroLabel12.Text = "Коэффициент избытка воздуха(alfa)";
            // 
            // TabPage3
            // 
            this.TabPage3.Controls.Add(this.dataGridMain);
            this.TabPage3.Controls.Add(this.metroButton9);
            this.TabPage3.Controls.Add(this.metroButton6);
            this.TabPage3.Controls.Add(this.unitsType);
            this.TabPage3.Controls.Add(this.metroLabel2);
            this.TabPage3.Controls.Add(this.metroLabel3);
            this.TabPage3.Controls.Add(this.metroLabel4);
            this.TabPage3.Controls.Add(this.metroLabel5);
            this.TabPage3.Controls.Add(this.QnTextBox);
            this.TabPage3.Controls.Add(this.Q5nomTextBox);
            this.TabPage3.Controls.Add(this.NnomTextBox);
            this.TabPage3.Controls.Add(this.QTextBox);
            this.TabPage3.HorizontalScrollbarBarColor = true;
            this.TabPage3.Location = new System.Drawing.Point(4, 35);
            this.TabPage3.Name = "TabPage3";
            this.TabPage3.Size = new System.Drawing.Size(542, 286);
            this.TabPage3.TabIndex = 1;
            this.TabPage3.VerticalScrollbarBarColor = true;
            // 
            // dataGridMain
            // 
            this.dataGridMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridMain.Location = new System.Drawing.Point(153, 246);
            this.dataGridMain.Name = "dataGridMain";
            this.dataGridMain.Size = new System.Drawing.Size(24, 22);
            this.dataGridMain.TabIndex = 41;
            this.dataGridMain.Visible = false;
            // 
            // metroButton9
            // 
            this.metroButton9.Location = new System.Drawing.Point(437, 246);
            this.metroButton9.Name = "metroButton9";
            this.metroButton9.Size = new System.Drawing.Size(61, 30);
            this.metroButton9.TabIndex = 40;
            this.metroButton9.Text = "Далее";
            this.metroButton9.Click += new System.EventHandler(this.ChangeTabGo);
            // 
            // metroButton6
            // 
            this.metroButton6.Location = new System.Drawing.Point(0, 246);
            this.metroButton6.Name = "metroButton6";
            this.metroButton6.Size = new System.Drawing.Size(64, 30);
            this.metroButton6.TabIndex = 34;
            this.metroButton6.Text = "Назад";
            this.metroButton6.Click += new System.EventHandler(this.ChangeTabBack);
            // 
            // unitsType
            // 
            this.unitsType.FontSize = MetroFramework.MetroLinkSize.Small;
            this.unitsType.FormattingEnabled = true;
            this.unitsType.ItemHeight = 19;
            this.unitsType.Items.AddRange(new object[] {
            "кВт/ч",
            "Гкалл/ч"});
            this.unitsType.Location = new System.Drawing.Point(298, 80);
            this.unitsType.Name = "unitsType";
            this.unitsType.Size = new System.Drawing.Size(97, 25);
            this.unitsType.TabIndex = 37;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(-4, 24);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(253, 19);
            this.metroLabel2.TabIndex = 33;
            this.metroLabel2.Text = "Низшая тпелотворная способность(Qn)";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(-4, 55);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(370, 19);
            this.metroLabel3.TabIndex = 34;
            this.metroLabel3.Text = "Номинальные потери тепла в окружающую среду(Q5nom)";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(-4, 86);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(277, 19);
            this.metroLabel4.TabIndex = 35;
            this.metroLabel4.Text = "Номинальная теплопроизводительность(Q)";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(-4, 117);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(230, 19);
            this.metroLabel5.TabIndex = 36;
            this.metroLabel5.Text = "Номинальное значение КПД(Nnom)";
            // 
            // QnTextBox
            // 
            this.QnTextBox.Location = new System.Drawing.Point(401, 18);
            this.QnTextBox.Name = "QnTextBox";
            this.QnTextBox.Size = new System.Drawing.Size(97, 25);
            this.QnTextBox.TabIndex = 29;
            this.QnTextBox.Text = "8100";
            this.QnTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyPoint_KeyPress);
            // 
            // Q5nomTextBox
            // 
            this.Q5nomTextBox.Location = new System.Drawing.Point(401, 49);
            this.Q5nomTextBox.Name = "Q5nomTextBox";
            this.Q5nomTextBox.Size = new System.Drawing.Size(97, 25);
            this.Q5nomTextBox.TabIndex = 30;
            this.Q5nomTextBox.Text = "0.9";
            this.Q5nomTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyPoint_KeyPress);
            // 
            // NnomTextBox
            // 
            this.NnomTextBox.Location = new System.Drawing.Point(401, 111);
            this.NnomTextBox.Name = "NnomTextBox";
            this.NnomTextBox.Size = new System.Drawing.Size(97, 25);
            this.NnomTextBox.TabIndex = 32;
            this.NnomTextBox.Text = "0.92";
            this.NnomTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyPoint_KeyPress);
            // 
            // QTextBox
            // 
            this.QTextBox.Location = new System.Drawing.Point(401, 80);
            this.QTextBox.Name = "QTextBox";
            this.QTextBox.Size = new System.Drawing.Size(97, 25);
            this.QTextBox.TabIndex = 31;
            this.QTextBox.Text = "1500";
            this.QTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyPoint_KeyPress);
            // 
            // TabPage4
            // 
            this.TabPage4.Controls.Add(this.Pvodout);
            this.TabPage4.Controls.Add(this.Pvodin);
            this.TabPage4.Controls.Add(this.metroLabel19);
            this.TabPage4.Controls.Add(this.metroLabel18);
            this.TabPage4.Controls.Add(this.NextStage);
            this.TabPage4.Controls.Add(this.DataViverButton);
            this.TabPage4.Controls.Add(this.metroButton5);
            this.TabPage4.Controls.Add(this.metroLabel6);
            this.TabPage4.Controls.Add(this.metroLabel7);
            this.TabPage4.Controls.Add(this.metroLabel8);
            this.TabPage4.Controls.Add(this.metroLabel9);
            this.TabPage4.Controls.Add(this.TvodvhTextBox);
            this.TabPage4.Controls.Add(this.GnomTextBox);
            this.TabPage4.Controls.Add(this.GvodTextBox);
            this.TabPage4.Controls.Add(this.GTextBox);
            this.TabPage4.HorizontalScrollbarBarColor = true;
            this.TabPage4.Location = new System.Drawing.Point(4, 35);
            this.TabPage4.Name = "TabPage4";
            this.TabPage4.Size = new System.Drawing.Size(542, 286);
            this.TabPage4.TabIndex = 2;
            this.TabPage4.VerticalScrollbarBarColor = true;
            // 
            // Pvodout
            // 
            this.Pvodout.Location = new System.Drawing.Point(401, 173);
            this.Pvodout.Name = "Pvodout";
            this.Pvodout.Size = new System.Drawing.Size(97, 25);
            this.Pvodout.TabIndex = 45;
            this.Pvodout.Text = "3.4";
            // 
            // Pvodin
            // 
            this.Pvodin.Location = new System.Drawing.Point(401, 142);
            this.Pvodin.Name = "Pvodin";
            this.Pvodin.Size = new System.Drawing.Size(97, 25);
            this.Pvodin.TabIndex = 44;
            this.Pvodin.Text = "4.4";
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.Location = new System.Drawing.Point(-4, 179);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(226, 19);
            this.metroLabel19.TabIndex = 43;
            this.metroLabel19.Text = "Давление воды на выходе из котла";
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.Location = new System.Drawing.Point(-4, 148);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(210, 19);
            this.metroLabel18.TabIndex = 42;
            this.metroLabel18.Text = "Давление воды на входе в котел";
            // 
            // NextStage
            // 
            this.NextStage.Location = new System.Drawing.Point(368, 246);
            this.NextStage.Name = "NextStage";
            this.NextStage.Size = new System.Drawing.Size(130, 30);
            this.NextStage.TabIndex = 41;
            this.NextStage.Text = "Следующий режим";
            this.NextStage.Click += new System.EventHandler(this.NextStage_Click);
            // 
            // DataViverButton
            // 
            this.DataViverButton.Location = new System.Drawing.Point(265, 246);
            this.DataViverButton.Name = "DataViverButton";
            this.DataViverButton.Size = new System.Drawing.Size(97, 30);
            this.DataViverButton.TabIndex = 40;
            this.DataViverButton.Text = "Посмотреть";
            this.DataViverButton.Click += new System.EventHandler(this.LookAt);
            // 
            // metroButton5
            // 
            this.metroButton5.Location = new System.Drawing.Point(0, 246);
            this.metroButton5.Name = "metroButton5";
            this.metroButton5.Size = new System.Drawing.Size(64, 30);
            this.metroButton5.TabIndex = 33;
            this.metroButton5.Text = "Назад";
            this.metroButton5.Click += new System.EventHandler(this.ChangeTabBack);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(-4, 55);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(214, 19);
            this.metroLabel6.TabIndex = 23;
            this.metroLabel6.Text = "Номинальный расход газа(Gnom)";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(-4, 24);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(160, 19);
            this.metroLabel7.TabIndex = 24;
            this.metroLabel7.Text = "Расход воды, м3/ч(Gvod)";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(-4, 86);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(282, 19);
            this.metroLabel8.TabIndex = 25;
            this.metroLabel8.Text = "Температура воды на входе в котел (Tvodvh)";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(-4, 117);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(96, 19);
            this.metroLabel9.TabIndex = 26;
            this.metroLabel9.Text = "Расход газа(G)";
            // 
            // TvodvhTextBox
            // 
            this.TvodvhTextBox.Location = new System.Drawing.Point(401, 80);
            this.TvodvhTextBox.Name = "TvodvhTextBox";
            this.TvodvhTextBox.Size = new System.Drawing.Size(97, 25);
            this.TvodvhTextBox.TabIndex = 21;
            this.TvodvhTextBox.Text = "70";
            this.TvodvhTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyPoint_KeyPress);
            // 
            // GnomTextBox
            // 
            this.GnomTextBox.Location = new System.Drawing.Point(401, 49);
            this.GnomTextBox.Name = "GnomTextBox";
            this.GnomTextBox.Size = new System.Drawing.Size(97, 25);
            this.GnomTextBox.TabIndex = 20;
            this.GnomTextBox.Text = "0";
            this.GnomTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyPoint_KeyPress);
            // 
            // GvodTextBox
            // 
            this.GvodTextBox.Location = new System.Drawing.Point(401, 18);
            this.GvodTextBox.Name = "GvodTextBox";
            this.GvodTextBox.Size = new System.Drawing.Size(97, 25);
            this.GvodTextBox.TabIndex = 19;
            this.GvodTextBox.Text = "0";
            this.GvodTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyPoint_KeyPress);
            // 
            // GTextBox
            // 
            this.GTextBox.Location = new System.Drawing.Point(401, 111);
            this.GTextBox.Name = "GTextBox";
            this.GTextBox.Size = new System.Drawing.Size(97, 25);
            this.GTextBox.TabIndex = 22;
            this.GTextBox.Text = "80";
            this.GTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyPoint_KeyPress);
            // 
            // metroTextBox10
            // 
            this.metroTextBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTextBox10.Location = new System.Drawing.Point(312, 183);
            this.metroTextBox10.Name = "metroTextBox10";
            this.metroTextBox10.Size = new System.Drawing.Size(97, 14);
            this.metroTextBox10.TabIndex = 9;
            // 
            // ButtonDB
            // 
            this.ButtonDB.Location = new System.Drawing.Point(448, 31);
            this.ButtonDB.Name = "ButtonDB";
            this.ButtonDB.Size = new System.Drawing.Size(74, 23);
            this.ButtonDB.TabIndex = 2;
            this.ButtonDB.Text = "Работа с БД";
            this.ButtonDB.Click += new System.EventHandler(this.metroButton4_Click);
            // 
            // metroStyleManager1
            // 
            this.metroStyleManager1.Owner = this;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 405);
            this.Controls.Add(this.ButtonDB);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Resizable = false;
            this.Text = "KIPHelper";
            this.Theme = MetroFramework.MetroThemeStyle.Light;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            this.TabPage1.PerformLayout();
            this.TabPage2.ResumeLayout(false);
            this.TabPage2.PerformLayout();
            this.TabPage3.ResumeLayout(false);
            this.TabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMain)).EndInit();
            this.TabPage4.ResumeLayout(false);
            this.TabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private MetroFramework.Controls.MetroTabControl tabControl1;
        private MetroFramework.Controls.MetroTabPage TabPage1;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroToggle TypeBoiler;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox metroTextBox10;
        private MetroFramework.Controls.MetroTabPage TabPage3;
        private MetroFramework.Controls.MetroComboBox dT;
        private MetroFramework.Controls.MetroTextBox Stage;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroComboBox unitsType;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox QnTextBox;
        private MetroFramework.Controls.MetroTextBox Q5nomTextBox;
        private MetroFramework.Controls.MetroTextBox NnomTextBox;
        private MetroFramework.Controls.MetroTextBox QTextBox;
        private MetroFramework.Controls.MetroTabPage TabPage4;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroTextBox TvodvhTextBox;
        private MetroFramework.Controls.MetroTextBox GnomTextBox;
        private MetroFramework.Controls.MetroTextBox GvodTextBox;
        private MetroFramework.Controls.MetroTextBox GTextBox;
        private MetroFramework.Controls.MetroTabPage TabPage2;
        private MetroFramework.Controls.MetroTextBox AlfaTextBox;
        private MetroFramework.Controls.MetroTextBox TvgorTextBox;
        private MetroFramework.Controls.MetroTextBox TgTextBox;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroTextBox Nstages;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroButton metroButton5;
        private MetroFramework.Controls.MetroButton metroButton6;
        private MetroFramework.Controls.MetroButton metroButton7;
        private MetroFramework.Controls.MetroButton ButtonDB;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroButton DataViverButton;
        private MetroFramework.Controls.MetroButton metroButton9;
        private MetroFramework.Controls.MetroButton NextStage;
        private MetroFramework.Controls.MetroTextBox Pvodout;
        private MetroFramework.Controls.MetroTextBox Pvodin;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private MetroFramework.Controls.MetroTextBox Pvozd;
        private MetroFramework.Controls.MetroTextBox PGaz;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private System.Windows.Forms.DataGridView dataGridMain;
        private MetroFramework.Components.MetroStyleManager metroStyleManager1;
        private MetroFramework.Components.MetroStyleExtender metroStyleExtender1;
    }
}

